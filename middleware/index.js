/*
 *  Ловим все 500-е ошибки в одном месте
 */
const asyncMiddleware = fn => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch(next);
};

module.exports = asyncMiddleware;
