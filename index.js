const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const logger = require('morgan');

const port = 8000;

app.use(
  cors({
    methods: ['GET', 'POST', 'OPTIONS', 'PUT', 'DELETE'],
    allowedHeaders: ['Content-Type', 'X-XSS-Protection'],
  })
);

app.use(logger('dev'));

app.use(bodyParser.json());

(async function() {
  try {
    require('./routes')(app);

    app.listen(port, () => {
      console.log(`Running on ${port}`);
    });

    app.use((error, req, res, next) => {
      const status = error.status || 500;
      res.status(status).json({ data: error.message });
    });
  } catch (e) {
    console.error(e);
  }
})();
