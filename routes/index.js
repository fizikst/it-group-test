const operationsRoutes = require('./operations');

module.exports = function(app) {
  app.use('/operation', operationsRoutes);
};
