const express = require('express');

const router = express.Router();
const { Client } = require('pg');

const config = require('../common/config');
const asyncMiddleware = require('../middleware');

/**
 * Возвращает список операций с учетом deletedat IS NULL.
 * @param {object} req
 * @param {object} res
 * @param {object} next
 */
const getOperations = asyncMiddleware(async (req, res, next) => {
  const { body: { pool } } = req;
  const { searchQuery, limit, offset } = pool;
  const defaultLimit = 10;
  const defaultOffset = 0;
  const values = [];
  const where = {
    sender: [],
    senderCode: [],
    consignee: [],
    consigneeCode: [],
    operation: [],
    client: [],
  };

  const bindWhere = (prop, field, value, position) => {
    if (['name', 'code'].indexOf(field) > -1) {
      where[prop].push([field, `$${position}`].join(' LIKE '));
      values.push(value);
    }
    if (field === 'id' || field === 'o.id') {
      where[prop].push(
        [
          field,
          `(SELECT unnest(string_to_array ($${position}, ',')::integer[]))`,
        ].join(' IN ')
      );
      values.push(value);
    }
  };

  const genSubWhere = (type, table) =>
    `(SELECT * FROM ${table} ${
      where[type].length > 0 ? `WHERE ${where[type].join(' AND ')}` : ''
    })`;

  if (pool && searchQuery) {
    for (let i = 1; i <= searchQuery.length; i += 1) {
      const [key, value] = Object.entries(searchQuery[i - 1])[0];
      const division = key.search(/Code|Name|Id/i);
      const [entity, field] = [
        key.substring(0, division),
        key.substring(division, key.length),
      ];
      const prefix = entity === 'operation' ? 'o.' : '';

      switch (field) {
        case 'Id':
          bindWhere(entity, prefix + field.toLowerCase(), value.join(','), i);
          break;
        case 'Name':
          bindWhere(entity, field.toLowerCase(), value.toLowerCase(), i);
          break;
        case 'Code':
          bindWhere(`${entity}Code`, field.toLowerCase(), value.toLowerCase(), i);
          break;
        default:
          break;
      }
    }
  }

  const client = new Client(config.postgres);
  await client.connect();

  const query = `
    SELECT distinct
      o.id AS id,
      client.id AS clientId, client.name AS clientName,
      sendercode.code AS senderCode, sender.id AS senderId, sender.name AS senderName,  
      consigneecode.code AS consigneeCode, consignee.id AS consigneeId, consignee.name AS consigneeName
    FROM operation AS o 
    JOIN ${genSubWhere(
      'sender',
      'dealerlocation'
    )} AS sender ON o.senderid=sender.id 
    JOIN ${genSubWhere(
      'senderCode',
      'dealerlocationcode'
    )} AS sendercode ON o.senderid=sendercode.dealerlocationid and o.clientid=sendercode.contractorid
    JOIN ${genSubWhere(
      'consignee',
      'dealerlocation'
    )} AS consignee ON o.consigneeid = consignee.id
    JOIN ${genSubWhere(
      'consigneeCode',
      'dealerlocationcode'
    )} AS consigneecode ON o.consigneeid=consigneecode.dealerlocationid and o.clientid=consigneecode.contractorid
    JOIN ${genSubWhere('client', 'contractor')} AS client ON o.clientid=client.id 
    WHERE ${
      where.operation.length > 0 ? `${where.operation.join('')} AND ` : ''
    } o.deletedat IS NULL
    LIMIT ${limit || defaultLimit} OFFSET  ${offset || defaultOffset}
  `;

  const { rows } = await client.query(query, values);
  await client.end();

  res.send({ error: false, data: rows, pool });
});

router.post('/find', (req, res, next) => getOperations(req, res, next));

module.exports = router;
