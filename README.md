# Важно
Решение сделано изходя из структуры таблиц

```

...


CREATE TABLE dealerlocationcode (
	id SERIAL PRIMARY KEY NOT NULL,
	dealerlocationid INTEGER REFERENCES dealerlocation (id), --id точки загрузки/выгрузки
	contractorid INTEGER REFERENCES contractor (id), --id контрагента
	code VARCHAR(25) --код точки загрузки/выгрузки
)

...


```

### Установка зависимостей

```
npm i
```

### Установка настройки postgresql
```
nano common/config.js
```

### Запуск сервера

```
npm run api
```
