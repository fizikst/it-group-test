module.exports = {
  postgres: {
    host: 'localhost',
    port: 5432,
    user: 'postgres',
    password: '12345',
    database: 'itgroup',
    max: 10,
    idleTimeoutMillis: 30000,
  },
};
